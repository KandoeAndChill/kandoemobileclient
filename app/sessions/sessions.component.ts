import {Component, OnInit} from "@angular/core";
import {SessionsService} from "~/services/sessions.service";
import {Session} from "~/model/session.model";
import {setInterval} from "tns-core-modules/timer";
import {Router} from "@angular/router";
import * as dialogs from "tns-core-modules/ui/dialogs";

var appSettings = require('application-settings');

@Component({
    selector: "Sessions",
    moduleId: module.id,
    templateUrl: "./sessions.component.html",
    styleUrls: ['./sessions.component.css']
})
export class SessionsComponent implements OnInit {
    countOfRows: string;
    itemsToLoad = [];
    token: string;

    constructor(private sessionsService: SessionsService, private router: Router) {
    }

    ngOnInit(): void {
        this.token = appSettings.getString('token');
        this.getSessions();
        this.repeater;
    }

    getSessions() {
        this.sessionsService.getSessions(this.token).subscribe(
            result => {
                if (result.toString().split(',').length > 0) {
                    for (var i = 0; i < (result.toString().split(',').length); i++) {
                        var session = new Session();
                        session = result[i];
                        this.itemsToLoad.push(session);
                        this.createRows();
                    }
                } else {
                    dialogs.confirm({
                        title: "NO ACTIVE SESSIONS",
                        message: "You have to wait for an active session.",
                        okButtonText: "Go to home"
                    }).then(r => {
                        this.router.navigateByUrl('/home');
                    });
                }
            }, error => {
                dialogs.confirm({
                    title: "NO ACTIVE SESSIONS",
                    message: error.error.message,
                    okButtonText: "Go to login"
                }).then(r => {
                    this.router.navigateByUrl('/login');
                });
            }
        )
    }

    updateSessions(lengthOfList: number) {
        this.sessionsService.getSessions(this.token).subscribe(
            result => {
                if (result.toString().split(',').length > lengthOfList) {
                    this.itemsToLoad = [];
                    for (var i = 0; i < (result.toString().split(',').length); i++) {
                        var session = new Session();
                        session = result[i];
                        this.itemsToLoad.push(session);
                        this.createRows();
                    }
                }
            }, error => {
                if (error.status == 500) {
                    dialogs.confirm({
                        title: "TOKEN HAS EXPIRED",
                        message: "You have to log in again.",
                        okButtonText: "Go to login"
                    }).then(r => {
                        this.router.navigateByUrl('/login');
                    });
                } else {
                    dialogs.confirm({
                        title: "PLEASE WAIT",
                        message: error.error.message,
                        okButtonText: "OK"
                    })
                }
            });
    }

    createRows() {
        var count = "";
        for (let i = 0; i < ((this.itemsToLoad.length + 2) + 1); i++) {
            if (i == (this.itemsToLoad.length + 2)) {
                count = count + "*";
            } else {
                count = count + "*,";
            }
        }
        this.countOfRows = count;
    }

    repeater = setInterval(() => {
        this.updateSessions(this.itemsToLoad.length);
    }, 1000);

    startSession(sessionName: string, sessionId: number) {
        dialogs.confirm({
            title: "Start session",
            message: "Are you sure to start with session '" + sessionName + "'",
            okButtonText: "Start!",
            cancelButtonText: "Cancel"
        }).then(result => {
            if (result == true) {
                appSettings.setString('sessionId', sessionId.toString());
                this.router.navigateByUrl('/game');
            }
        });
    }

    goBack() {
        this.itemsToLoad = [];
        this.router.navigateByUrl('/home');
    }
}


