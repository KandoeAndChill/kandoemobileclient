import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import {SessionsComponent} from "~/sessions/sessions.component";

@NgModule({
    imports: [
        NativeScriptCommonModule
    ],
    declarations: [
        SessionsComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class SessionsModule { }

