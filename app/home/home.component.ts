import { Component, OnInit, AfterViewInit, ViewChild, ElementRef } from "@angular/core";
import { RouterExtensions } from 'nativescript-angular';
import { AnimationCurve} from "ui/enums";
import * as dialogs from "tns-core-modules/ui/dialogs";
import {Page} from "tns-core-modules/ui/page";
import {Router} from "@angular/router";

var appSettings = require('application-settings')



@Component({
    selector: "Home",
    moduleId: module.id,
    templateUrl: "./home.component.html",
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, AfterViewInit {
    @ViewChild("menuContainer") menuContainer: ElementRef;
    menuIsOpen: boolean = false;
    currentPath: string;

    constructor(private page: Page, private router: Router) {
        this.page.enableSwipeBackNavigation = false;
    }

    ngOnInit(): void {
        console.log(appSettings.getString('token'), appSettings.getString('email'));
    }

    ngAfterViewInit(): void {
        this.initializeMenu();
    }

    navigateToPath(path: string) {
        // close menu if the path is the same as current path or path is empty (close has empty path)
        if (!path || path === this.currentPath) {
            this.closeMenu();
        }
        else {
            this.router.navigate([path]).then(
                () => {
                    this.closeMenu();
                    this.currentPath = path;
                });
        }
    }

    initializeMenu() {
        // set the origin point for the rotation
        this.menuContainer.nativeElement.originX = 0;
        this.menuContainer.nativeElement.originY = 0;

        this.menuIsOpen = true;
    }

    openMenu() {
        this.menuContainer.nativeElement.animate({
            rotate: 0,
            curve: AnimationCurve.cubicBezier(1, .02, .45, .93),
            duration: 200
        }).then(
            () => {
                this.menuIsOpen = true;
            });
    }

    closeMenu() {
        this.menuContainer.nativeElement.animate({
            rotate: - 90,
            curve: AnimationCurve.cubicBezier(1, .02, .45, .93),
            duration: 200
        }).then(
            () => {
                this.menuIsOpen = false;
            });
    }

    navigateToSessions() {
        this.router.navigateByUrl('/sessions');
    }

    navigateToProfile() {
        this.router.navigateByUrl('/profile');
    }
    logOut(){
        dialogs.confirm({
            title: "Log out",
            message: "Are you sure?",
            okButtonText: "Log out",
            cancelButtonText: "Cancel"
        }).then(result => {
            // result argument is boolean
            if (result == true){
                this.router.navigateByUrl('/login');
            }
        });
    }
}
