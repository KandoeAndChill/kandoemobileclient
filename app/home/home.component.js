"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var nativescript_angular_1 = require("nativescript-angular");
var enums_1 = require("ui/enums");
var dialogs = require("tns-core-modules/ui/dialogs");
var page_1 = require("tns-core-modules/ui/page");
var appSettings = require('application-settings');
var HomeComponent = /** @class */ (function () {
    function HomeComponent(page, routerExtensions) {
        this.page = page;
        this.routerExtensions = routerExtensions;
        this.pageName = "DASHBOARD";
        this.menuIsOpen = false;
        this.menuItems = [
            { name: 'Profile', path: '/profile', color: '#3AEFBA' },
            { name: 'Start Game', path: '/game', color: '#72DEFF' },
            { name: 'Settings', path: '/settings', color: '#FEDC78' },
        ];
        this.page.enableSwipeBackNavigation = false;
    }
    HomeComponent.prototype.ngOnInit = function () {
        console.log(appSettings.getString('token'), appSettings.getString('email'));
    };
    HomeComponent.prototype.ngAfterViewInit = function () {
        this.initializeMenu();
    };
    HomeComponent.prototype.navigateToPath = function (path) {
        var _this = this;
        // close menu if the path is the same as current path or path is empty (close has empty path)
        if (!path || path === this.currentPath) {
            this.closeMenu();
        }
        else {
            this.routerExtensions.navigate([path]).then(function () {
                _this.closeMenu();
                _this.currentPath = path;
            });
        }
    };
    HomeComponent.prototype.initializeMenu = function () {
        // set the origin point for the rotation
        this.menuContainer.nativeElement.originX = 0;
        this.menuContainer.nativeElement.originY = 0;
        this.menuIsOpen = true;
    };
    HomeComponent.prototype.openMenu = function () {
        var _this = this;
        this.menuContainer.nativeElement.animate({
            rotate: 0,
            curve: enums_1.AnimationCurve.cubicBezier(1, .02, .45, .93),
            duration: 200
        }).then(function () {
            _this.menuIsOpen = true;
        });
    };
    HomeComponent.prototype.closeMenu = function () {
        var _this = this;
        this.menuContainer.nativeElement.animate({
            rotate: -90,
            curve: enums_1.AnimationCurve.cubicBezier(1, .02, .45, .93),
            duration: 200
        }).then(function () {
            _this.menuIsOpen = false;
        });
    };
    HomeComponent.prototype.navigateToHome = function () {
        //Home
    };
    HomeComponent.prototype.navigateToSessions = function () {
        this.routerExtensions.navigateByUrl('/sessions');
        /* dialogs.prompt({
             title: "Start new session",
             message: "Voer sessionId in.",
             okButtonText: "Start session!",
             cancelButtonText: "Cancel",
             inputType: dialogs.inputType.number
         }).then(r => {
             var appSettings = require('application-settings')
             appSettings.setString('sessionId', r.text);

             this.routerExtensions.navigateByUrl('/game');
         });*/
    };
    HomeComponent.prototype.navigateToSettings = function () {
        //Settings
    };
    HomeComponent.prototype.navigateToProfile = function () {
        this.routerExtensions.navigateByUrl('/profile');
    };
    HomeComponent.prototype.logOut = function () {
        var _this = this;
        dialogs.confirm({
            title: "Log out",
            message: "Are you sure?",
            okButtonText: "Log out",
            cancelButtonText: "Cancel"
        }).then(function (result) {
            // result argument is boolean
            if (result == true) {
                _this.routerExtensions.navigateByUrl('/login');
            }
        });
    };
    __decorate([
        core_1.ViewChild("menuContainer"),
        __metadata("design:type", core_1.ElementRef)
    ], HomeComponent.prototype, "menuContainer", void 0);
    HomeComponent = __decorate([
        core_1.Component({
            selector: "Home",
            moduleId: module.id,
            templateUrl: "./home.component.html",
            styleUrls: ['./home.component.css']
        }),
        __metadata("design:paramtypes", [page_1.Page, nativescript_angular_1.RouterExtensions])
    ], HomeComponent);
    return HomeComponent;
}());
exports.HomeComponent = HomeComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaG9tZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJob21lLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUF3RjtBQUN4Riw2REFBd0Q7QUFDeEQsa0NBQW1FO0FBQ25FLHFEQUF1RDtBQUN2RCxpREFBOEM7QUFFOUMsSUFBSSxXQUFXLEdBQUcsT0FBTyxDQUFDLHNCQUFzQixDQUFDLENBQUE7QUFVakQ7SUFhSSx1QkFBb0IsSUFBVSxFQUFVLGdCQUFrQztRQUF0RCxTQUFJLEdBQUosSUFBSSxDQUFNO1FBQVUscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQVgxRSxhQUFRLEdBQVcsV0FBVyxDQUFBO1FBRTlCLGVBQVUsR0FBWSxLQUFLLENBQUM7UUFDNUIsY0FBUyxHQUFvRDtZQUN6RCxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsSUFBSSxFQUFFLFVBQVUsRUFBRSxLQUFLLEVBQUUsU0FBUyxFQUFFO1lBQ3ZELEVBQUUsSUFBSSxFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxTQUFTLEVBQUU7WUFDdkQsRUFBRSxJQUFJLEVBQUUsVUFBVSxFQUFFLElBQUksRUFBRSxXQUFXLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBRTtTQUM1RCxDQUFDO1FBS0UsSUFBSSxDQUFDLElBQUksQ0FBQyx5QkFBeUIsR0FBRyxLQUFLLENBQUM7SUFFaEQsQ0FBQztJQUVELGdDQUFRLEdBQVI7UUFDSSxPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLEVBQUUsV0FBVyxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO0lBQ2hGLENBQUM7SUFFRCx1Q0FBZSxHQUFmO1FBQ0ksSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO0lBQzFCLENBQUM7SUFFRCxzQ0FBYyxHQUFkLFVBQWUsSUFBWTtRQUEzQixpQkFZQztRQVhHLDZGQUE2RjtRQUM3RixJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksS0FBSyxJQUFJLENBQUMsV0FBVyxFQUFFO1lBQ3BDLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztTQUNwQjthQUNJO1lBQ0QsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUN2QztnQkFDSSxLQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7Z0JBQ2pCLEtBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO1lBQzVCLENBQUMsQ0FBQyxDQUFDO1NBQ1Y7SUFDTCxDQUFDO0lBRUQsc0NBQWMsR0FBZDtRQUNJLHdDQUF3QztRQUN4QyxJQUFJLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDO1FBQzdDLElBQUksQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUM7UUFFN0MsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7SUFDM0IsQ0FBQztJQUVELGdDQUFRLEdBQVI7UUFBQSxpQkFTQztRQVJHLElBQUksQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQztZQUNyQyxNQUFNLEVBQUUsQ0FBQztZQUNULEtBQUssRUFBRSxzQkFBYyxDQUFDLFdBQVcsQ0FBQyxDQUFDLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLENBQUM7WUFDbkQsUUFBUSxFQUFFLEdBQUc7U0FDaEIsQ0FBQyxDQUFDLElBQUksQ0FDSDtZQUNJLEtBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO1FBQzNCLENBQUMsQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQUVELGlDQUFTLEdBQVQ7UUFBQSxpQkFTQztRQVJHLElBQUksQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQztZQUNyQyxNQUFNLEVBQUUsQ0FBRSxFQUFFO1lBQ1osS0FBSyxFQUFFLHNCQUFjLENBQUMsV0FBVyxDQUFDLENBQUMsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsQ0FBQztZQUNuRCxRQUFRLEVBQUUsR0FBRztTQUNoQixDQUFDLENBQUMsSUFBSSxDQUNIO1lBQ0ksS0FBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7UUFDNUIsQ0FBQyxDQUFDLENBQUM7SUFDWCxDQUFDO0lBRUQsc0NBQWMsR0FBZDtRQUNHLE1BQU07SUFDVCxDQUFDO0lBQ0QsMENBQWtCLEdBQWxCO1FBQ0ksSUFBSSxDQUFDLGdCQUFnQixDQUFDLGFBQWEsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUNqRDs7Ozs7Ozs7Ozs7Y0FXTTtJQUNWLENBQUM7SUFDRCwwQ0FBa0IsR0FBbEI7UUFDSSxVQUFVO0lBQ2QsQ0FBQztJQUNELHlDQUFpQixHQUFqQjtRQUNJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLENBQUM7SUFDcEQsQ0FBQztJQUNELDhCQUFNLEdBQU47UUFBQSxpQkFZQztRQVhHLE9BQU8sQ0FBQyxPQUFPLENBQUM7WUFDWixLQUFLLEVBQUUsU0FBUztZQUNoQixPQUFPLEVBQUUsZUFBZTtZQUN4QixZQUFZLEVBQUUsU0FBUztZQUN2QixnQkFBZ0IsRUFBRSxRQUFRO1NBQzdCLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxNQUFNO1lBQ1YsNkJBQTZCO1lBQzdCLElBQUksTUFBTSxJQUFJLElBQUksRUFBQztnQkFDZixLQUFJLENBQUMsZ0JBQWdCLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2FBQ2pEO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBdkcyQjtRQUEzQixnQkFBUyxDQUFDLGVBQWUsQ0FBQztrQ0FBZ0IsaUJBQVU7d0RBQUM7SUFIN0MsYUFBYTtRQU56QixnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLE1BQU07WUFDaEIsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFdBQVcsRUFBRSx1QkFBdUI7WUFDcEMsU0FBUyxFQUFFLENBQUMsc0JBQXNCLENBQUM7U0FDdEMsQ0FBQzt5Q0FjNEIsV0FBSSxFQUE0Qix1Q0FBZ0I7T0FiakUsYUFBYSxDQTJHekI7SUFBRCxvQkFBQztDQUFBLEFBM0dELElBMkdDO0FBM0dZLHNDQUFhIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIEFmdGVyVmlld0luaXQsIFZpZXdDaGlsZCwgRWxlbWVudFJlZiB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBSb3V0ZXJFeHRlbnNpb25zIH0gZnJvbSAnbmF0aXZlc2NyaXB0LWFuZ3VsYXInO1xuaW1wb3J0IHtBbmRyb2lkQWN0aW9uSXRlbVBvc2l0aW9uLCBBbmltYXRpb25DdXJ2ZX0gZnJvbSBcInVpL2VudW1zXCI7XG5pbXBvcnQgKiBhcyBkaWFsb2dzIGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL3VpL2RpYWxvZ3NcIjtcbmltcG9ydCB7UGFnZX0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvcGFnZVwiO1xuXG52YXIgYXBwU2V0dGluZ3MgPSByZXF1aXJlKCdhcHBsaWNhdGlvbi1zZXR0aW5ncycpXG5cblxuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogXCJIb21lXCIsXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcbiAgICB0ZW1wbGF0ZVVybDogXCIuL2hvbWUuY29tcG9uZW50Lmh0bWxcIixcbiAgICBzdHlsZVVybHM6IFsnLi9ob21lLmNvbXBvbmVudC5jc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBIb21lQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBBZnRlclZpZXdJbml0IHtcblxuICAgIHBhZ2VOYW1lOiBzdHJpbmcgPSBcIkRBU0hCT0FSRFwiXG4gICAgQFZpZXdDaGlsZChcIm1lbnVDb250YWluZXJcIikgbWVudUNvbnRhaW5lcjogRWxlbWVudFJlZjtcbiAgICBtZW51SXNPcGVuOiBib29sZWFuID0gZmFsc2U7XG4gICAgbWVudUl0ZW1zOiB7IG5hbWU6IHN0cmluZywgcGF0aDogc3RyaW5nLCBjb2xvcjogc3RyaW5nIH1bXSA9IFtcbiAgICAgICAgeyBuYW1lOiAnUHJvZmlsZScsIHBhdGg6ICcvcHJvZmlsZScsIGNvbG9yOiAnIzNBRUZCQScgfSxcbiAgICAgICAgeyBuYW1lOiAnU3RhcnQgR2FtZScsIHBhdGg6ICcvZ2FtZScsIGNvbG9yOiAnIzcyREVGRicgfSxcbiAgICAgICAgeyBuYW1lOiAnU2V0dGluZ3MnLCBwYXRoOiAnL3NldHRpbmdzJywgY29sb3I6ICcjRkVEQzc4JyB9LFxuICAgIF07XG5cbiAgICBjdXJyZW50UGF0aDogc3RyaW5nO1xuXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBwYWdlOiBQYWdlLCBwcml2YXRlIHJvdXRlckV4dGVuc2lvbnM6IFJvdXRlckV4dGVuc2lvbnMpIHtcbiAgICAgICAgdGhpcy5wYWdlLmVuYWJsZVN3aXBlQmFja05hdmlnYXRpb24gPSBmYWxzZTtcblxuICAgIH1cblxuICAgIG5nT25Jbml0KCk6IHZvaWQge1xuICAgICAgICBjb25zb2xlLmxvZyhhcHBTZXR0aW5ncy5nZXRTdHJpbmcoJ3Rva2VuJyksIGFwcFNldHRpbmdzLmdldFN0cmluZygnZW1haWwnKSk7XG4gICAgfVxuXG4gICAgbmdBZnRlclZpZXdJbml0KCk6IHZvaWQge1xuICAgICAgICB0aGlzLmluaXRpYWxpemVNZW51KCk7XG4gICAgfVxuXG4gICAgbmF2aWdhdGVUb1BhdGgocGF0aDogc3RyaW5nKSB7XG4gICAgICAgIC8vIGNsb3NlIG1lbnUgaWYgdGhlIHBhdGggaXMgdGhlIHNhbWUgYXMgY3VycmVudCBwYXRoIG9yIHBhdGggaXMgZW1wdHkgKGNsb3NlIGhhcyBlbXB0eSBwYXRoKVxuICAgICAgICBpZiAoIXBhdGggfHwgcGF0aCA9PT0gdGhpcy5jdXJyZW50UGF0aCkge1xuICAgICAgICAgICAgdGhpcy5jbG9zZU1lbnUoKTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbcGF0aF0pLnRoZW4oXG4gICAgICAgICAgICAgICAgKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmNsb3NlTWVudSgpO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmN1cnJlbnRQYXRoID0gcGF0aDtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGluaXRpYWxpemVNZW51KCkge1xuICAgICAgICAvLyBzZXQgdGhlIG9yaWdpbiBwb2ludCBmb3IgdGhlIHJvdGF0aW9uXG4gICAgICAgIHRoaXMubWVudUNvbnRhaW5lci5uYXRpdmVFbGVtZW50Lm9yaWdpblggPSAwO1xuICAgICAgICB0aGlzLm1lbnVDb250YWluZXIubmF0aXZlRWxlbWVudC5vcmlnaW5ZID0gMDtcblxuICAgICAgICB0aGlzLm1lbnVJc09wZW4gPSB0cnVlO1xuICAgIH1cblxuICAgIG9wZW5NZW51KCkge1xuICAgICAgICB0aGlzLm1lbnVDb250YWluZXIubmF0aXZlRWxlbWVudC5hbmltYXRlKHtcbiAgICAgICAgICAgIHJvdGF0ZTogMCxcbiAgICAgICAgICAgIGN1cnZlOiBBbmltYXRpb25DdXJ2ZS5jdWJpY0JlemllcigxLCAuMDIsIC40NSwgLjkzKSxcbiAgICAgICAgICAgIGR1cmF0aW9uOiAyMDBcbiAgICAgICAgfSkudGhlbihcbiAgICAgICAgICAgICgpID0+IHtcbiAgICAgICAgICAgICAgICB0aGlzLm1lbnVJc09wZW4gPSB0cnVlO1xuICAgICAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgY2xvc2VNZW51KCkge1xuICAgICAgICB0aGlzLm1lbnVDb250YWluZXIubmF0aXZlRWxlbWVudC5hbmltYXRlKHtcbiAgICAgICAgICAgIHJvdGF0ZTogLSA5MCxcbiAgICAgICAgICAgIGN1cnZlOiBBbmltYXRpb25DdXJ2ZS5jdWJpY0JlemllcigxLCAuMDIsIC40NSwgLjkzKSxcbiAgICAgICAgICAgIGR1cmF0aW9uOiAyMDBcbiAgICAgICAgfSkudGhlbihcbiAgICAgICAgICAgICgpID0+IHtcbiAgICAgICAgICAgICAgICB0aGlzLm1lbnVJc09wZW4gPSBmYWxzZTtcbiAgICAgICAgICAgIH0pO1xuICAgIH1cblxuICAgIG5hdmlnYXRlVG9Ib21lKCkge1xuICAgICAgIC8vSG9tZVxuICAgIH1cbiAgICBuYXZpZ2F0ZVRvU2Vzc2lvbnMoKSB7XG4gICAgICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZUJ5VXJsKCcvc2Vzc2lvbnMnKTtcbiAgICAgICAgLyogZGlhbG9ncy5wcm9tcHQoe1xuICAgICAgICAgICAgIHRpdGxlOiBcIlN0YXJ0IG5ldyBzZXNzaW9uXCIsXG4gICAgICAgICAgICAgbWVzc2FnZTogXCJWb2VyIHNlc3Npb25JZCBpbi5cIixcbiAgICAgICAgICAgICBva0J1dHRvblRleHQ6IFwiU3RhcnQgc2Vzc2lvbiFcIixcbiAgICAgICAgICAgICBjYW5jZWxCdXR0b25UZXh0OiBcIkNhbmNlbFwiLFxuICAgICAgICAgICAgIGlucHV0VHlwZTogZGlhbG9ncy5pbnB1dFR5cGUubnVtYmVyXG4gICAgICAgICB9KS50aGVuKHIgPT4ge1xuICAgICAgICAgICAgIHZhciBhcHBTZXR0aW5ncyA9IHJlcXVpcmUoJ2FwcGxpY2F0aW9uLXNldHRpbmdzJylcbiAgICAgICAgICAgICBhcHBTZXR0aW5ncy5zZXRTdHJpbmcoJ3Nlc3Npb25JZCcsIHIudGV4dCk7XG5cbiAgICAgICAgICAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGVCeVVybCgnL2dhbWUnKTtcbiAgICAgICAgIH0pOyovXG4gICAgfVxuICAgIG5hdmlnYXRlVG9TZXR0aW5ncygpIHtcbiAgICAgICAgLy9TZXR0aW5nc1xuICAgIH1cbiAgICBuYXZpZ2F0ZVRvUHJvZmlsZSgpIHtcbiAgICAgICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlQnlVcmwoJy9wcm9maWxlJyk7XG4gICAgfVxuICAgIGxvZ091dCgpe1xuICAgICAgICBkaWFsb2dzLmNvbmZpcm0oe1xuICAgICAgICAgICAgdGl0bGU6IFwiTG9nIG91dFwiLFxuICAgICAgICAgICAgbWVzc2FnZTogXCJBcmUgeW91IHN1cmU/XCIsXG4gICAgICAgICAgICBva0J1dHRvblRleHQ6IFwiTG9nIG91dFwiLFxuICAgICAgICAgICAgY2FuY2VsQnV0dG9uVGV4dDogXCJDYW5jZWxcIlxuICAgICAgICB9KS50aGVuKHJlc3VsdCA9PiB7XG4gICAgICAgICAgICAvLyByZXN1bHQgYXJndW1lbnQgaXMgYm9vbGVhblxuICAgICAgICAgICAgaWYgKHJlc3VsdCA9PSB0cnVlKXtcbiAgICAgICAgICAgICAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGVCeVVybCgnL2xvZ2luJyk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH1cbn1cbiJdfQ==