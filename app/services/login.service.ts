import { Injectable } from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable()
export class LoginService {
    url = "http://192.168.1.120:8080";

    constructor(private http: HttpClient) { }

    login(userIdentifier: String, password: String) {
        //Smartphone & PC moet verbonden zijn met dezelfde WIFI-netwerk
        //Private IP-adres ipv localhost(cmd -> ifconfig | grep "inet " | grep -v 127.0.0.1)
        return this.http.post(this.url + '/users/signin',
            {userIdentifier, password},
            {headers: new HttpHeaders().set('Content-Type', 'application/json').set('Accept', 'text/plain'), responseType: 'text'
            });
    }

    register(email: String, username: String, password: String) {
        return this.http.post('http://192.168.1.120:8080/users/save',
            {email,username,password},
            {headers: new HttpHeaders().set('Content-Type', 'application/json').set('Accept', 'text/plain'), responseType: 'text'
        });
    }
}
