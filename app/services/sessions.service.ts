import { Injectable } from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable()
export class SessionsService {
    url = 'http://192.168.1.120:8080';
    constructor(private http: HttpClient) { }

    getSessions(token: string){
        return  this.http.get(this.url + "/sessions/findAllActiveSessionsOfUser",
            {headers: new HttpHeaders()
                    .set('Accept', 'application/json')
                    .set('Authorization',"Bearer " +  token)
                , responseType: 'json'});
    }
}






