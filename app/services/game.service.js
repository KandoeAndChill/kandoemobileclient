"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var appSettings = require('application-settings');
var GameService = /** @class */ (function () {
    function GameService(http) {
        this.http = http;
        this.url = "http://192.168.1.120:";
    }
    GameService.prototype.getCards = function (token, id) {
        return this.http.get(this.url + "8080/cardSessions/findAllCardSessionsBySession/" + id, { headers: new http_1.HttpHeaders()
                .set('Accept', 'application/json')
                .set('Authorization', "Bearer " + token),
            responseType: 'json' });
    };
    GameService.prototype.getSession = function (token, id) {
        return this.http.get(this.url + "8080/sessions/findGameSession/" + id, { headers: new http_1.HttpHeaders()
                .set('Accept', 'application/json')
                .set('Authorization', "Bearer " + token),
            responseType: 'json' });
    };
    GameService.prototype.makeTurn = function (token, id) {
        return this.http.put("http://192.168.1.120:8080/sessions/turn/" + id, null, {
            headers: new http_1.HttpHeaders()
                .set('Accept', 'application/json')
                .set('Authorization', "Bearer " + token)
        });
    };
    GameService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], GameService);
    return GameService;
}());
exports.GameService = GameService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2FtZS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiZ2FtZS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQTJDO0FBQzNDLDZDQUE2RDtBQUM3RCxJQUFJLFdBQVcsR0FBRyxPQUFPLENBQUMsc0JBQXNCLENBQUMsQ0FBQTtBQUdqRDtJQUdJLHFCQUFvQixJQUFnQjtRQUFoQixTQUFJLEdBQUosSUFBSSxDQUFZO1FBRnBDLFFBQUcsR0FBRyx1QkFBdUIsQ0FBQztJQUVVLENBQUM7SUFFekMsOEJBQVEsR0FBUixVQUFTLEtBQWEsRUFBRSxFQUFVO1FBQzlCLE9BQVEsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEdBQUcsR0FBRyxpREFBaUQsR0FBRyxFQUFFLEVBQy9FLEVBQUMsT0FBTyxFQUFFLElBQUksa0JBQVcsRUFBRTtpQkFDbEIsR0FBRyxDQUFDLFFBQVEsRUFBRSxrQkFBa0IsQ0FBQztpQkFDakMsR0FBRyxDQUFDLGVBQWUsRUFBQyxTQUFTLEdBQUksS0FBSyxDQUFDO1lBQ3pDLFlBQVksRUFBRSxNQUFNLEVBQUMsQ0FBQyxDQUFDO0lBQzFDLENBQUM7SUFFRCxnQ0FBVSxHQUFWLFVBQVcsS0FBYSxFQUFFLEVBQVU7UUFDaEMsT0FBUSxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsR0FBRyxHQUFHLGdDQUFnQyxHQUFHLEVBQUUsRUFDbEUsRUFBQyxPQUFPLEVBQUUsSUFBSSxrQkFBVyxFQUFFO2lCQUNsQixHQUFHLENBQUMsUUFBUSxFQUFFLGtCQUFrQixDQUFDO2lCQUNqQyxHQUFHLENBQUMsZUFBZSxFQUFDLFNBQVMsR0FBSSxLQUFLLENBQUM7WUFDMUMsWUFBWSxFQUFFLE1BQU0sRUFBQyxDQUFDLENBQUM7SUFDckMsQ0FBQztJQUVELDhCQUFRLEdBQVIsVUFBUyxLQUFhLEVBQUUsRUFBVTtRQUM5QixPQUFRLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLDBDQUEwQyxHQUFHLEVBQUUsRUFBQyxJQUFJLEVBQUM7WUFDdkUsT0FBTyxFQUFFLElBQUksa0JBQVcsRUFBRTtpQkFDckIsR0FBRyxDQUFDLFFBQVEsRUFBRSxrQkFBa0IsQ0FBQztpQkFDakMsR0FBRyxDQUFDLGVBQWUsRUFBQyxTQUFTLEdBQUksS0FBSyxDQUFDO1NBQy9DLENBQUUsQ0FBQztJQUNSLENBQUM7SUEzQlEsV0FBVztRQUR2QixpQkFBVSxFQUFFO3lDQUlpQixpQkFBVTtPQUgzQixXQUFXLENBNEJ2QjtJQUFELGtCQUFDO0NBQUEsQUE1QkQsSUE0QkM7QUE1Qlksa0NBQVciLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7SHR0cENsaWVudCwgSHR0cEhlYWRlcnN9IGZyb20gXCJAYW5ndWxhci9jb21tb24vaHR0cFwiO1xudmFyIGFwcFNldHRpbmdzID0gcmVxdWlyZSgnYXBwbGljYXRpb24tc2V0dGluZ3MnKVxuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgR2FtZVNlcnZpY2Uge1xuICAgIHVybCA9IFwiaHR0cDovLzE5Mi4xNjguMS4xMjA6XCI7XG5cbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGh0dHA6IEh0dHBDbGllbnQpIHsgfVxuXG4gICAgZ2V0Q2FyZHModG9rZW46IHN0cmluZywgaWQ6IG51bWJlcil7XG4gICAgICAgIHJldHVybiAgdGhpcy5odHRwLmdldCh0aGlzLnVybCArIFwiODA4MC9jYXJkU2Vzc2lvbnMvZmluZEFsbENhcmRTZXNzaW9uc0J5U2Vzc2lvbi9cIiArIGlkLFxuICAgICAgICAgICAgICAgIHtoZWFkZXJzOiBuZXcgSHR0cEhlYWRlcnMoKVxuICAgICAgICAgICAgICAgICAgICAgICAgLnNldCgnQWNjZXB0JywgJ2FwcGxpY2F0aW9uL2pzb24nKVxuICAgICAgICAgICAgICAgICAgICAgICAgLnNldCgnQXV0aG9yaXphdGlvbicsXCJCZWFyZXIgXCIgKyAgdG9rZW4pXG4gICAgICAgICAgICAgICAgICAgICAsIHJlc3BvbnNlVHlwZTogJ2pzb24nfSk7XG4gICAgfVxuXG4gICAgZ2V0U2Vzc2lvbih0b2tlbjogc3RyaW5nLCBpZDogbnVtYmVyKXtcbiAgICAgICAgcmV0dXJuICB0aGlzLmh0dHAuZ2V0KHRoaXMudXJsICsgXCI4MDgwL3Nlc3Npb25zL2ZpbmRHYW1lU2Vzc2lvbi9cIiArIGlkLFxuICAgICAgICAgICAge2hlYWRlcnM6IG5ldyBIdHRwSGVhZGVycygpXG4gICAgICAgICAgICAgICAgICAgIC5zZXQoJ0FjY2VwdCcsICdhcHBsaWNhdGlvbi9qc29uJylcbiAgICAgICAgICAgICAgICAgICAgLnNldCgnQXV0aG9yaXphdGlvbicsXCJCZWFyZXIgXCIgKyAgdG9rZW4pXG4gICAgICAgICAgICAgICAgLCByZXNwb25zZVR5cGU6ICdqc29uJ30pO1xuICAgIH1cblxuICAgIG1ha2VUdXJuKHRva2VuOiBzdHJpbmcsIGlkOiBudW1iZXIpe1xuICAgICAgICByZXR1cm4gIHRoaXMuaHR0cC5wdXQoXCJodHRwOi8vMTkyLjE2OC4xLjEyMDo4MDgwL3Nlc3Npb25zL3R1cm4vXCIgKyBpZCxudWxsLHtcbiAgICAgICAgICAgIGhlYWRlcnM6IG5ldyBIdHRwSGVhZGVycygpXG4gICAgICAgICAgICAgICAgLnNldCgnQWNjZXB0JywgJ2FwcGxpY2F0aW9uL2pzb24nKVxuICAgICAgICAgICAgICAgIC5zZXQoJ0F1dGhvcml6YXRpb24nLFwiQmVhcmVyIFwiICsgIHRva2VuKVxuICAgICAgICB9ICk7XG4gICAgfVxufVxuIl19