import { Injectable } from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable()
export class GameService {
    url = "http://192.168.1.120:8080";

    constructor(private http: HttpClient) { }

    getCards(token: string, id: number){
        return  this.http.get(this.url + '/cardSessions/findAllCardSessionsBySession/' + id,
                {headers: new HttpHeaders()
                        .set('Accept', 'application/json')
                        .set('Authorization',"Bearer " +  token)
                     , responseType: 'json'});
    }

    getSession(token: string, id: number){
        return  this.http.get(this.url + '/sessions/findGameSession/' + id,
            {headers: new HttpHeaders()
                    .set('Accept', 'application/json')
                    .set('Authorization',"Bearer " +  token)
                , responseType: 'json'});
    }

    makeTurn(token: string, id: number){
        return  this.http.put(this.url + '/sessions/turn/' + id,null,{
            headers: new HttpHeaders()
                .set('Accept', 'application/json')
                .set('Authorization',"Bearer " +  token)
        } );
    }
}
