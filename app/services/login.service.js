"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var LoginService = /** @class */ (function () {
    function LoginService(http) {
        this.http = http;
    }
    LoginService.prototype.login = function (userIdentifier, password) {
        //Smartphone & PC moet verbonden zijn met dezelfde WIFI-netwerk
        //Private IP-adres ipv localhost(cmd -> ifconfig | grep "inet " | grep -v 127.0.0.1)
        return this.http.post('http://192.168.1.120:8080/users/signin', { userIdentifier: userIdentifier, password: password }, { headers: new http_1.HttpHeaders().set('Content-Type', 'application/json').set('Accept', 'text/plain'), responseType: 'text'
        });
    };
    LoginService.prototype.register = function (email, username, password) {
        return this.http.post('http://192.168.1.120:8080/users/save', { email: email, username: username, password: password }, { headers: new http_1.HttpHeaders().set('Content-Type', 'application/json').set('Accept', 'text/plain'), responseType: 'text'
        });
    };
    LoginService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], LoginService);
    return LoginService;
}());
exports.LoginService = LoginService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImxvZ2luLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMkM7QUFDM0MsNkNBQTZEO0FBRzdEO0lBQ0ksc0JBQW9CLElBQWdCO1FBQWhCLFNBQUksR0FBSixJQUFJLENBQVk7SUFBSSxDQUFDO0lBRXpDLDRCQUFLLEdBQUwsVUFBTSxjQUFzQixFQUFFLFFBQWdCO1FBQzFDLCtEQUErRDtRQUMvRCxvRkFBb0Y7UUFDcEYsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyx3Q0FBd0MsRUFDMUQsRUFBQyxjQUFjLGdCQUFBLEVBQUUsUUFBUSxVQUFBLEVBQUMsRUFDMUIsRUFBQyxPQUFPLEVBQUUsSUFBSSxrQkFBVyxFQUFFLENBQUMsR0FBRyxDQUFDLGNBQWMsRUFBRSxrQkFBa0IsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsWUFBWSxDQUFDLEVBQUUsWUFBWSxFQUFFLE1BQU07U0FDcEgsQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQUdELCtCQUFRLEdBQVIsVUFBUyxLQUFhLEVBQUUsUUFBZ0IsRUFBRSxRQUFnQjtRQUN0RCxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLHNDQUFzQyxFQUN4RCxFQUFDLEtBQUssT0FBQSxFQUFDLFFBQVEsVUFBQSxFQUFDLFFBQVEsVUFBQSxFQUFDLEVBQ3pCLEVBQUMsT0FBTyxFQUFFLElBQUksa0JBQVcsRUFBRSxDQUFDLEdBQUcsQ0FBQyxjQUFjLEVBQUUsa0JBQWtCLENBQUMsQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLFlBQVksQ0FBQyxFQUFFLFlBQVksRUFBRSxNQUFNO1NBQ3hILENBQUMsQ0FBQztJQUNQLENBQUM7SUFsQlEsWUFBWTtRQUR4QixpQkFBVSxFQUFFO3lDQUVpQixpQkFBVTtPQUQzQixZQUFZLENBbUJ4QjtJQUFELG1CQUFDO0NBQUEsQUFuQkQsSUFtQkM7QUFuQlksb0NBQVkiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7SHR0cENsaWVudCwgSHR0cEhlYWRlcnN9IGZyb20gXCJAYW5ndWxhci9jb21tb24vaHR0cFwiO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgTG9naW5TZXJ2aWNlIHtcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGh0dHA6IEh0dHBDbGllbnQpIHsgfVxuXG4gICAgbG9naW4odXNlcklkZW50aWZpZXI6IFN0cmluZywgcGFzc3dvcmQ6IFN0cmluZykge1xuICAgICAgICAvL1NtYXJ0cGhvbmUgJiBQQyBtb2V0IHZlcmJvbmRlbiB6aWpuIG1ldCBkZXplbGZkZSBXSUZJLW5ldHdlcmtcbiAgICAgICAgLy9Qcml2YXRlIElQLWFkcmVzIGlwdiBsb2NhbGhvc3QoY21kIC0+IGlmY29uZmlnIHwgZ3JlcCBcImluZXQgXCIgfCBncmVwIC12IDEyNy4wLjAuMSlcbiAgICAgICAgcmV0dXJuIHRoaXMuaHR0cC5wb3N0KCdodHRwOi8vMTkyLjE2OC4xLjEyMDo4MDgwL3VzZXJzL3NpZ25pbicsXG4gICAgICAgICAgICB7dXNlcklkZW50aWZpZXIsIHBhc3N3b3JkfSxcbiAgICAgICAgICAgIHtoZWFkZXJzOiBuZXcgSHR0cEhlYWRlcnMoKS5zZXQoJ0NvbnRlbnQtVHlwZScsICdhcHBsaWNhdGlvbi9qc29uJykuc2V0KCdBY2NlcHQnLCAndGV4dC9wbGFpbicpLCByZXNwb25zZVR5cGU6ICd0ZXh0J1xuICAgICAgICAgICAgfSk7XG4gICAgfVxuXG5cbiAgICByZWdpc3RlcihlbWFpbDogU3RyaW5nLCB1c2VybmFtZTogU3RyaW5nLCBwYXNzd29yZDogU3RyaW5nKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmh0dHAucG9zdCgnaHR0cDovLzE5Mi4xNjguMS4xMjA6ODA4MC91c2Vycy9zYXZlJyxcbiAgICAgICAgICAgIHtlbWFpbCx1c2VybmFtZSxwYXNzd29yZH0sXG4gICAgICAgICAgICB7aGVhZGVyczogbmV3IEh0dHBIZWFkZXJzKCkuc2V0KCdDb250ZW50LVR5cGUnLCAnYXBwbGljYXRpb24vanNvbicpLnNldCgnQWNjZXB0JywgJ3RleHQvcGxhaW4nKSwgcmVzcG9uc2VUeXBlOiAndGV4dCdcbiAgICAgICAgfSk7XG4gICAgfVxufVxuIl19