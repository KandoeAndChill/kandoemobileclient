import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { NativeScriptFormsModule } from "nativescript-angular/forms";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";

import {HomeComponent} from "~/home/home.component";
import {GameComponent} from "~/game/game.component";
import {ProfileComponent} from "~/profile/profile.component";
import {LoginComponent } from "./login/login.component";

import {SessionsComponent} from "~/sessions/sessions.component";

import {GameService} from "~/services/game.service";
import {LoginService } from "./services/login.service";
import {SessionsService } from "./services/sessions.service";

@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        NativeScriptModule,
        NativeScriptFormsModule,
        NativeScriptHttpClientModule,
        AppRoutingModule,
    ],
    declarations: [
        AppComponent,
        LoginComponent,
        HomeComponent,
        GameComponent,
        ProfileComponent,
        SessionsComponent
    ],
    providers: [
        LoginService, GameService, SessionsService
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class AppModule { }
