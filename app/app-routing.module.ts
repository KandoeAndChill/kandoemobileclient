import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { LoginComponent } from './login/login.component';
import {HomeComponent} from "~/home/home.component";
import {ProfileComponent} from "~/profile/profile.component";
import {GameComponent} from "~/game/game.component";
import {SessionsComponent} from "~/sessions/sessions.component";


const routes: Routes = [
    { path: "", redirectTo: "/login", pathMatch: "full" },
    { path: "login", component: LoginComponent },
    { path: "home", component:HomeComponent },
    { path: "profile", component: ProfileComponent},
    { path: "game", component: GameComponent },
    { path: "sessions", component: SessionsComponent },
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})

export class AppRoutingModule {}
