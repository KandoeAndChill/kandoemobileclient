import {Component, ElementRef, OnInit, ViewChild} from "@angular/core";
import { Router } from "@angular/router";
import { alert } from "tns-core-modules/ui/dialogs";
import { Page } from "tns-core-modules/ui/page";
import { User } from "../model/user.model";
import { LoginService } from "../services/login.service";

var appSettings = require('application-settings')

@Component({
    selector: "app-login",
    moduleId: module.id,
    templateUrl: "./login.component.html",
    styleUrls: ['./login.component.css']
})
export class LoginComponent  implements OnInit {
    isLoggingIn = true;
    user: User;

    @ViewChild("username") username: ElementRef;
    @ViewChild("email") email: ElementRef;
    @ViewChild("password") password: ElementRef;
    @ViewChild("confirmPassword") confirmPassword: ElementRef;

    constructor(private page: Page, private loginService: LoginService, private router: Router) {
        this.page.actionBarHidden = true;
        this.page.enableSwipeBackNavigation = false;
        this.user = new User();
    }

    ngOnInit(): void {

    }

    toggleForm() {
        this.isLoggingIn = !this.isLoggingIn;
    }

    public submit() {
        if(this.isLoggingIn){
            if(this.email.nativeElement.text == "" ){
                alert("email leeg");
            }else if(this.password.nativeElement.text == "" ) {
                alert("passw leeg");
            }else{
                this.loginService.login(this.user.email,this.user.password).subscribe(
                     token => {
                         appSettings.setString('token', token);
                         appSettings.setString('username', this.Base64.atob(token));
                         appSettings.setString('email', this.user.email);
                         appSettings.setString('password', this.user.password);
                         this.router.navigateByUrl('/home')

                         },
                     error => {
                         alert(error.statusText)
                     });
            }
        }else{
                this.loginService.register(this.user.email, this.user.username,this.user.password).subscribe(
                register => {
                    alert("User created!");
                },
                error => {
                    alert(error.statusText)
                });
        }
    }

    chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
    Base64 = {
        atob: (input:string = '') => {
            let splitInput = input.split('.')[1]
            let str = splitInput.replace(/=+$/, '');
            let output = '';

            if (str.length % 4 == 1) {
                throw new Error("'atob' failed: The string to be decoded is not correctly encoded.");
            }
            for (let bc = 0, bs = 0, buffer, i = 0;
                 buffer = str.charAt(i++);

                 ~buffer && (bs = bc % 4 ? bs * 64 + buffer : buffer,
                 bc++ % 4) ? output += String.fromCharCode(255 & bs >> (-2 * bc & 6)) : 0
            ) {
                buffer = this.chars.indexOf(buffer);
            }
            let result = output.split(",")[0].split(":")[1];
            return result.substr(1, result.length-2);
        }
    };
}

