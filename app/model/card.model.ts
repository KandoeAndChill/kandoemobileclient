export class Card {
    private _id: number;
    private _imageUrl: string;
    private _cardText: string;
    private _themeId: number;
    private _color: string;
    private _priority: number;
    private _sessionId: number;
    private _labelId: string;

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get imageUrl(): string {
        return this._imageUrl;
    }

    set imageUrl(value: string) {
        this._imageUrl = value;
    }

    get cardText(): string {
        return this._cardText;
    }

    set cardText(value: string) {
        this._cardText = value;
    }

    get themeId(): number {
        return this._themeId;
    }

    set themeId(value: number) {
        this._themeId = value;
    }

    get color(): string {
        return this._color;
    }

    set color(value: string) {
        this._color = value;
    }

    get priority(): number {
        return this._priority;
    }

    set priority(value: number) {
        this._priority = value;
    }

    get sessionId(): number {
        return this._sessionId;
    }

    set sessionId(value: number) {
        this._sessionId = value;
    }

    get labelId(): string {
        return this._labelId;
    }

    set labelId(value: string) {
        this._labelId = value;
    }
}
