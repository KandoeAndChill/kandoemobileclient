
export class Session {
   id: number;
   name: string;
   chanceOfCircles: boolean;
   minCards: number;
   maxCards: number;
   themeId: number;
   startTime: string;
   userCardsEnabled: boolean;
   numberOfRounds: number;
   roundTimeInMinutes: number;
}
