import {Card} from "~/model/card.model";

export class GameSession {
     private _id: number;
     private _name: string;
     private _gameOver: boolean;
     private _activeUserId: number;
     private _cardList: Card[];
     private _numberOfRounds: number;
     private _currentRound: number;

     get id(): number {
          return this._id;
     }

     set id(value: number) {
          this._id = value;
     }

     get name(): string {
          return this._name;
     }

     set name(value: string) {
          this._name = value;
     }

     get gameOver(): boolean {
          return this._gameOver;
     }

     set gameOver(value: boolean) {
          this._gameOver = value;
     }

     get activeUserId(): number {
          return this._activeUserId;
     }

     set activeUserId(value: number) {
          this._activeUserId = value;
     }

     get cardList(): Card[] {
          return this._cardList;
     }

     set cardList(value: Card[]) {
          this._cardList = value;
     }

     get numberOfRounds(): number {
          return this._numberOfRounds;
     }

     set numberOfRounds(value: number) {
          this._numberOfRounds = value;
     }

     get currentRound(): number {
          return this._currentRound;
     }

     set currentRound(value: number) {
          this._currentRound = value;
     }
}

