import {Component, ElementRef, OnInit, ViewChild} from "@angular/core";
import {Router} from "@angular/router";

let imagepicker = require("nativescript-imagepicker");
var appSettings = require('application-settings');

@Component({
    selector: "Profile",
    moduleId: module.id,
    templateUrl: "./profile.component.html",
    styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
    imageAssets = [];
    imageSrc: any;
    isSingleMode: boolean = true;

    @ViewChild("image") image: ElementRef;
    @ViewChild("email") email: ElementRef;
    @ViewChild("username") username: ElementRef;

    constructor(private router: Router) {
    }

    ngOnInit(): void {
        var usernameTxt = appSettings.getString("username");
        var emailTxt = appSettings.getString("email");
        var passwordTxt = appSettings.getString("password");

        this.email.nativeElement.text = emailTxt;
        this.username.nativeElement.text = usernameTxt;
    }

    goBack(){
       this.router.navigateByUrl('/home')
    }

    onSelectSingleTap() {
        this.isSingleMode = true;

        let context = imagepicker.create({
            mode: "single"
        });
        this.startSelection(context);
    }

    startSelection(context) {
        let that = this;

        context
            .authorize()
            .then(() => {
                that.imageAssets = [];
                that.imageSrc = null;
                return context.present();
            })
            .then((selection) => {
                console.log("Selection done: " + JSON.stringify(selection));
                that.imageSrc = that.isSingleMode && selection.length > 0 ? selection[0] : null;

                // set the images to be loaded from the assets with optimal sizes (optimize memory usage)
                that.imageAssets = selection;
            }).catch(function (e) {
            console.log(e);
        });
    }
}
