import {Component, OnInit, ViewContainerRef} from "@angular/core";
import {Page} from "tns-core-modules/ui/page";
import {Router} from "@angular/router";
import {ModalDialogService} from "nativescript-angular/modal-dialog";
import {GameService} from "~/services/game.service";
import {setInterval} from "tns-core-modules/timer";
import {Card} from "~/model/card.model";
import * as dialogs from "tns-core-modules/ui/dialogs";

var appSettings = require('application-settings');
var view = require("ui/core/view");

@Component({
    selector: "Game",
    moduleId: module.id,
    templateUrl: "./game.component.html",
    styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {
    token: string;
    countOfRows: string;
    itemsToLoad = [];
    cardList = [];
    stayOnSession = true;
    sessionId: number;

    constructor(private page: Page, private router: Router, private modalService: ModalDialogService, private viewContainerRef: ViewContainerRef, private gameService: GameService) {
        this.page.enableSwipeBackNavigation = false;
    }

    createRows() {
        var count = "";
        for (let i = 0; i < ((this.itemsToLoad.length * 2) + 1); i++) {
            if (i == (this.itemsToLoad.length*2)) {
                count = count + "*";
            } else {
                count = count + "*,";
            }
        }
        this.countOfRows = count;
    }

    click(menuId: number) {
        this.gameService.makeTurn(this.token, this.itemsToLoad[menuId].id).subscribe(
            result => {
                this.animateCard(menuId, result["cardList"][menuId]["priority"]);
            }, error => {
                if(error.status == 500){
                    dialogs.confirm({
                        title: "TOKEN HAS EXPIRED",
                        message: "You have to log in again.",
                        okButtonText: "Go to login"
                    }).then(r => {
                        this.router.navigateByUrl('/login');
                    });
                }else{
                    dialogs.confirm({
                        title: "PLEASE WAIT",
                        message: error.error.message,
                        okButtonText: "OK"
                    })
                }
            }
        );
    }
    ngOnInit(): void {
        this.token = appSettings.getString("token");
        this.sessionId = appSettings.getString('sessionId');
        this.getCards(this.sessionId);
        this.repeater;
    }

    repeater = setInterval(() => {
        if(this.stayOnSession){
            this.updateSession(this.sessionId);
        }
    }, 3000);


    getCards(sessionId: number) {
        this.gameService.getSession(this.token, this.sessionId).subscribe(result => {
            if(result["cardList"].toString().split(',').length > 1){
                for (let i = 0; i < result["cardList"].toString().split(',').length; i++) {
                    var card = new Card;
                    card = result["cardList"][i];
                    card.labelId = "card" + i;
                    card.color = this.colorchanger();
                    this.itemsToLoad.push(card);
                }
                this.createRows()
            }else{
                dialogs.confirm({
                    title: "NO CARDS SELECTED",
                    message: "You cannot start a session without cards.",
                    okButtonText: "Go to home"
                }).then(r => {
                    this.router.navigateByUrl('/home');
                });
            }}, error =>{
            if(error.status == 500){
                dialogs.confirm({
                    title: "TOKEN HAS EXPIRED",
                    message: "You have to log in again.",
                    okButtonText: "Go to login"
                }).then(r => {
                    this.router.navigateByUrl('/login');
                });
            }else{
                dialogs.confirm({
                    title: "PLEASE WAIT",
                    message: error.error.message,
                    okButtonText: "OK"
                })
            }
        });
    }

    updateSession(sessionId: number){
        this.gameService.getSession(this.token, sessionId).subscribe(result => {
            if(result["gameOver"]){
                this.stayOnSession = false;
                dialogs.prompt({
                    title: "Game over!",
                    message: "Max priority has been reached.",
                    okButtonText: "End session"
                }).then(r => {
                    this.router.navigateByUrl('/home');
                });
            }else{
                for (let i = 0; i < this.itemsToLoad.length; i++) {
                    this.animateCard(i,result["cardList"][i]["priority"]);
                    if(result["cardList"][i]["priority"] > this.itemsToLoad[i].priority){
                         this.animateCard(i,result["cardList"][i]["priority"]);
                         this.itemsToLoad[i].priority = result["cardList"][i]["priority"];
                     }
                }
            }
        }, error => {
            if(error.status == 500){
                dialogs.confirm({
                    title: "TOKEN HAS EXPIRED",
                    message: "You have to log in again.",
                    okButtonText: "Go to login"
                }).then(r => {
                    this.router.navigateByUrl('/login');
                });
            }else{
                dialogs.confirm({
                    title: "PLEASE WAIT",
                    message: error.error.message,
                    okButtonText: "OK"
                })
            }
        });
    }

    colorchanger(): string{
        return "rgb(" + Math.round(Math.random() * 250) + "," + Math.round(Math.random() * 250)+"," + Math.round(Math.random() * 250) +")";
    }

    animateCard(listId: number, priority: number){
        view.getViewById(this.page, this.itemsToLoad[listId].labelId).animate({
            translate: {x: (priority * 15) , y: 0},
            duration: 1000,
        });
    }

    stopSession(){
        dialogs.confirm({
            title: "Stop session",
            message: "Are you sure?",
            okButtonText: "Go to login",
            cancelButtonText: "Cancel"
        }).then(result => {
            if (result == true){
                this.router.navigateByUrl('/home');
            }
        });
    }
}









