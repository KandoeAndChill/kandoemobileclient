"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var page_1 = require("tns-core-modules/ui/page");
var router_1 = require("@angular/router");
var modal_dialog_1 = require("nativescript-angular/modal-dialog");
var game_service_1 = require("~/services/game.service");
var timer_1 = require("tns-core-modules/timer");
var card_model_1 = require("~/model/card.model");
var dialogs = require("tns-core-modules/ui/dialogs");
var gameSession_model_1 = require("~/model/gameSession.model");
var nativescript_angular_1 = require("nativescript-angular");
var appSettings = require('application-settings');
var view = require("ui/core/view");
var GameComponent = /** @class */ (function () {
    function GameComponent(routerExtensions, page, router, modalService, viewContainerRef, gameService) {
        var _this = this;
        this.routerExtensions = routerExtensions;
        this.page = page;
        this.router = router;
        this.modalService = modalService;
        this.viewContainerRef = viewContainerRef;
        this.gameService = gameService;
        this.itemsToLoad = [];
        this.cardList = [];
        this.stayOnSession = true;
        this.gameSession = new gameSession_model_1.GameSession();
        this.repeater = timer_1.setInterval(function () {
            if (_this.stayOnSession) {
                _this.updateSession(_this.sessionId);
            }
        }, 3000);
        this.page.enableSwipeBackNavigation = false;
    }
    GameComponent.prototype.createRows = function () {
        var count = "";
        for (var i = 0; i < ((this.itemsToLoad.length * 2) + 1); i++) {
            if (i == (this.itemsToLoad.length * 2)) {
                count = count + "*";
            }
            else {
                count = count + "*,";
            }
        }
        this.countOfRows = count;
    };
    GameComponent.prototype.click = function (menuId) {
        var _this = this;
        this.gameService.makeTurn(this.token, this.itemsToLoad[menuId].id).subscribe(function (result) {
            _this.animateCard(menuId, result["cardList"][menuId]["priority"]);
        }, function (error) {
            if (error.status == 500) {
                dialogs.confirm({
                    title: "TOKEN HAS EXPIRED",
                    message: "You have to log in again.",
                    okButtonText: "Go to login"
                }).then(function (r) {
                    _this.router.navigateByUrl('/login');
                });
            }
            else {
                dialogs.confirm({
                    title: "PLEASE WAIT",
                    message: error.error.message,
                    okButtonText: "OK"
                });
            }
        });
    };
    GameComponent.prototype.ngOnInit = function () {
        this.token = appSettings.getString("token");
        this.sessionId = appSettings.getString('sessionId');
        this.getCards(this.sessionId);
        this.repeater;
    };
    GameComponent.prototype.getCards = function (sessionId) {
        var _this = this;
        this.gameService.getSession(this.token, this.sessionId).subscribe(function (result) {
            console.log(result["cardList"]);
            if (result["cardList"].toString().split(',').length > 1) {
                for (var i = 0; i < result["cardList"].toString().split(',').length; i++) {
                    var card = new card_model_1.Card;
                    card = result["cardList"][i];
                    card.labelId = "card" + i;
                    card.color = _this.colorchanger();
                    _this.itemsToLoad.push(card);
                }
                _this.createRows();
            }
            else {
                dialogs.confirm({
                    title: "NO CARDS SELECTED",
                    message: "You cannot start a session without cards.",
                    okButtonText: "Go to home"
                }).then(function (r) {
                    _this.router.navigateByUrl('/home');
                });
            }
        }, function (error) {
            if (error.status == 500) {
                dialogs.confirm({
                    title: "TOKEN HAS EXPIRED",
                    message: "You have to log in again.",
                    okButtonText: "Go to login"
                }).then(function (r) {
                    _this.router.navigateByUrl('/login');
                });
            }
            else {
                dialogs.confirm({
                    title: "PLEASE WAIT",
                    message: error.error.message,
                    okButtonText: "OK"
                });
            }
        });
    };
    GameComponent.prototype.updateSession = function (sessionId) {
        var _this = this;
        this.gameService.getSession(this.token, sessionId).subscribe(function (result) {
            if (result["gameOver"]) {
                _this.stayOnSession = false;
                dialogs.prompt({
                    title: "Game over!",
                    message: "Max priority has been reached.",
                    okButtonText: "End session"
                }).then(function (r) {
                    _this.router.navigateByUrl('/home');
                });
            }
            else {
                for (var i = 0; i < _this.itemsToLoad.length; i++) {
                    _this.animateCard(i, result["cardList"][i]["priority"]);
                    if (result["cardList"][i]["priority"] > _this.itemsToLoad[i].priority) {
                        _this.animateCard(i, result["cardList"][i]["priority"]);
                        _this.itemsToLoad[i].priority = result["cardList"][i]["priority"];
                    }
                }
            }
        }, function (error) {
            if (error.status == 500) {
                dialogs.confirm({
                    title: "TOKEN HAS EXPIRED",
                    message: "You have to log in again.",
                    okButtonText: "Go to login"
                }).then(function (r) {
                    _this.router.navigateByUrl('/login');
                });
            }
            else {
                dialogs.confirm({
                    title: "PLEASE WAIT",
                    message: error.error.message,
                    okButtonText: "OK"
                });
            }
        });
    };
    GameComponent.prototype.colorchanger = function () {
        return "rgb(" + Math.round(Math.random() * 250) + "," + Math.round(Math.random() * 250) + "," + Math.round(Math.random() * 250) + ")";
    };
    GameComponent.prototype.animateCard = function (listId, priority) {
        view.getViewById(this.page, this.itemsToLoad[listId].labelId).animate({
            translate: { x: (priority * 15), y: 0 },
            duration: 1000,
        });
    };
    GameComponent.prototype.stopSession = function () {
        var _this = this;
        dialogs.confirm({
            title: "Stop session",
            message: "Are you sure?",
            okButtonText: "Go to login",
            cancelButtonText: "Cancel"
        }).then(function (result) {
            // result argument is boolean
            if (result == true) {
                _this.routerExtensions.navigateByUrl('/home');
            }
        });
    };
    GameComponent = __decorate([
        core_1.Component({
            selector: "Game",
            moduleId: module.id,
            templateUrl: "./game.component.html",
            styleUrls: ['./game.component.css']
        }),
        __metadata("design:paramtypes", [nativescript_angular_1.RouterExtensions, page_1.Page, router_1.Router, modal_dialog_1.ModalDialogService, core_1.ViewContainerRef, game_service_1.GameService])
    ], GameComponent);
    return GameComponent;
}());
exports.GameComponent = GameComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2FtZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJnYW1lLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUF3RztBQUN4RyxpREFBOEM7QUFDOUMsMENBQXVDO0FBQ3ZDLGtFQUF5RjtBQUN6Rix3REFBb0Q7QUFDcEQsZ0RBQWtFO0FBQ2xFLGlEQUF3QztBQUV4QyxxREFBdUQ7QUFDdkQsK0RBQXNEO0FBQ3RELDZEQUFzRDtBQUV0RCxJQUFJLFdBQVcsR0FBRyxPQUFPLENBQUMsc0JBQXNCLENBQUMsQ0FBQztBQUNsRCxJQUFJLElBQUksR0FBRyxPQUFPLENBQUMsY0FBYyxDQUFDLENBQUM7QUFRbkM7SUFVSSx1QkFBb0IsZ0JBQWtDLEVBQVUsSUFBVSxFQUFVLE1BQWMsRUFBVSxZQUFnQyxFQUFVLGdCQUFrQyxFQUFVLFdBQXdCO1FBQTFOLGlCQUVDO1FBRm1CLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFBVSxTQUFJLEdBQUosSUFBSSxDQUFNO1FBQVUsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUFVLGlCQUFZLEdBQVosWUFBWSxDQUFvQjtRQUFVLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFBVSxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQVAxTixnQkFBVyxHQUFHLEVBQUUsQ0FBQztRQUNqQixhQUFRLEdBQUcsRUFBRSxDQUFDO1FBQ2Qsa0JBQWEsR0FBRyxJQUFJLENBQUM7UUFFckIsZ0JBQVcsR0FBRyxJQUFJLCtCQUFXLEVBQUUsQ0FBQztRQWlEaEMsYUFBUSxHQUFHLG1CQUFXLENBQUM7WUFDbkIsSUFBRyxLQUFJLENBQUMsYUFBYSxFQUFDO2dCQUNsQixLQUFJLENBQUMsYUFBYSxDQUFDLEtBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQzthQUN0QztRQUNMLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQWpETCxJQUFJLENBQUMsSUFBSSxDQUFDLHlCQUF5QixHQUFHLEtBQUssQ0FBQztJQUNoRCxDQUFDO0lBRUQsa0NBQVUsR0FBVjtRQUNJLElBQUksS0FBSyxHQUFHLEVBQUUsQ0FBQztRQUNmLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDMUQsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sR0FBQyxDQUFDLENBQUMsRUFBRTtnQkFDbEMsS0FBSyxHQUFHLEtBQUssR0FBRyxHQUFHLENBQUM7YUFDdkI7aUJBQU07Z0JBQ0gsS0FBSyxHQUFHLEtBQUssR0FBRyxJQUFJLENBQUM7YUFDeEI7U0FDSjtRQUNELElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO0lBQzdCLENBQUM7SUFFRCw2QkFBSyxHQUFMLFVBQU0sTUFBYztRQUFwQixpQkFzQkM7UUFyQkcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLFNBQVMsQ0FDeEUsVUFBQSxNQUFNO1lBQ0YsS0FBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7UUFDckUsQ0FBQyxFQUFFLFVBQUEsS0FBSztZQUNKLElBQUcsS0FBSyxDQUFDLE1BQU0sSUFBSSxHQUFHLEVBQUM7Z0JBQ25CLE9BQU8sQ0FBQyxPQUFPLENBQUM7b0JBQ1osS0FBSyxFQUFFLG1CQUFtQjtvQkFDMUIsT0FBTyxFQUFFLDJCQUEyQjtvQkFDcEMsWUFBWSxFQUFFLGFBQWE7aUJBQzlCLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxDQUFDO29CQUNMLEtBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUN4QyxDQUFDLENBQUMsQ0FBQzthQUNOO2lCQUFJO2dCQUNELE9BQU8sQ0FBQyxPQUFPLENBQUM7b0JBQ1osS0FBSyxFQUFFLGFBQWE7b0JBQ3BCLE9BQU8sRUFBRSxLQUFLLENBQUMsS0FBSyxDQUFDLE9BQU87b0JBQzVCLFlBQVksRUFBRSxJQUFJO2lCQUNyQixDQUFDLENBQUE7YUFDTDtRQUNMLENBQUMsQ0FDSixDQUFDO0lBQ04sQ0FBQztJQUNELGdDQUFRLEdBQVI7UUFDSSxJQUFJLENBQUMsS0FBSyxHQUFHLFdBQVcsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDNUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxXQUFXLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ3BELElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQzlCLElBQUksQ0FBQyxRQUFRLENBQUM7SUFDbEIsQ0FBQztJQVNELGdDQUFRLEdBQVIsVUFBUyxTQUFpQjtRQUExQixpQkFzQ0M7UUFyQ0csSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsTUFBTTtZQUNwRSxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO1lBQ2hDLElBQUcsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDLFFBQVEsRUFBRSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFDO2dCQUNuRCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDLFFBQVEsRUFBRSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7b0JBQ3RFLElBQUksSUFBSSxHQUFHLElBQUksaUJBQUksQ0FBQztvQkFDcEIsSUFBSSxHQUFHLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDN0IsSUFBSSxDQUFDLE9BQU8sR0FBRyxNQUFNLEdBQUcsQ0FBQyxDQUFDO29CQUMxQixJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztvQkFFakMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7aUJBQy9CO2dCQUNELEtBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQTthQUNwQjtpQkFBSTtnQkFDRCxPQUFPLENBQUMsT0FBTyxDQUFDO29CQUNaLEtBQUssRUFBRSxtQkFBbUI7b0JBQzFCLE9BQU8sRUFBRSwyQ0FBMkM7b0JBQ3BELFlBQVksRUFBRSxZQUFZO2lCQUM3QixDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsQ0FBQztvQkFDTCxLQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFDdkMsQ0FBQyxDQUFDLENBQUM7YUFDTjtRQUFBLENBQUMsRUFBRSxVQUFBLEtBQUs7WUFDVCxJQUFHLEtBQUssQ0FBQyxNQUFNLElBQUksR0FBRyxFQUFDO2dCQUNuQixPQUFPLENBQUMsT0FBTyxDQUFDO29CQUNaLEtBQUssRUFBRSxtQkFBbUI7b0JBQzFCLE9BQU8sRUFBRSwyQkFBMkI7b0JBQ3BDLFlBQVksRUFBRSxhQUFhO2lCQUM5QixDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsQ0FBQztvQkFDTCxLQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDeEMsQ0FBQyxDQUFDLENBQUM7YUFDTjtpQkFBSTtnQkFDRCxPQUFPLENBQUMsT0FBTyxDQUFDO29CQUNaLEtBQUssRUFBRSxhQUFhO29CQUNwQixPQUFPLEVBQUUsS0FBSyxDQUFDLEtBQUssQ0FBQyxPQUFPO29CQUM1QixZQUFZLEVBQUUsSUFBSTtpQkFDckIsQ0FBQyxDQUFBO2FBQ0w7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxxQ0FBYSxHQUFiLFVBQWMsU0FBaUI7UUFBL0IsaUJBcUNDO1FBcENHLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsU0FBUyxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsTUFBTTtZQUMvRCxJQUFHLE1BQU0sQ0FBQyxVQUFVLENBQUMsRUFBQztnQkFDbEIsS0FBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7Z0JBQzNCLE9BQU8sQ0FBQyxNQUFNLENBQUM7b0JBQ1gsS0FBSyxFQUFFLFlBQVk7b0JBQ25CLE9BQU8sRUFBRSxnQ0FBZ0M7b0JBQ3pDLFlBQVksRUFBRSxhQUFhO2lCQUM5QixDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsQ0FBQztvQkFDTCxLQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFDdkMsQ0FBQyxDQUFDLENBQUM7YUFDTjtpQkFBSTtnQkFDRCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7b0JBQzlDLEtBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxFQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO29CQUN0RCxJQUFHLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsR0FBRyxLQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsRUFBQzt3QkFDL0QsS0FBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEVBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7d0JBQ3RELEtBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxHQUFHLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQztxQkFDcEU7aUJBQ0w7YUFDSjtRQUNMLENBQUMsRUFBRSxVQUFBLEtBQUs7WUFDSixJQUFHLEtBQUssQ0FBQyxNQUFNLElBQUksR0FBRyxFQUFDO2dCQUNuQixPQUFPLENBQUMsT0FBTyxDQUFDO29CQUNaLEtBQUssRUFBRSxtQkFBbUI7b0JBQzFCLE9BQU8sRUFBRSwyQkFBMkI7b0JBQ3BDLFlBQVksRUFBRSxhQUFhO2lCQUM5QixDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsQ0FBQztvQkFDTCxLQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDeEMsQ0FBQyxDQUFDLENBQUM7YUFDTjtpQkFBSTtnQkFDRCxPQUFPLENBQUMsT0FBTyxDQUFDO29CQUNaLEtBQUssRUFBRSxhQUFhO29CQUNwQixPQUFPLEVBQUUsS0FBSyxDQUFDLEtBQUssQ0FBQyxPQUFPO29CQUM1QixZQUFZLEVBQUUsSUFBSTtpQkFDckIsQ0FBQyxDQUFBO2FBQ0w7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxvQ0FBWSxHQUFaO1FBQ0ksT0FBTyxNQUFNLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsR0FBRyxDQUFDLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFHLEdBQUcsQ0FBQyxHQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxHQUFHLENBQUMsR0FBRSxHQUFHLENBQUM7SUFDdkksQ0FBQztJQUVELG1DQUFXLEdBQVgsVUFBWSxNQUFjLEVBQUUsUUFBZ0I7UUFDeEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsT0FBTyxDQUFDO1lBQ2xFLFNBQVMsRUFBRSxFQUFDLENBQUMsRUFBRSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUMsRUFBRyxDQUFDLEVBQUUsQ0FBQyxFQUFDO1lBQ3RDLFFBQVEsRUFBRSxJQUFJO1NBQ2pCLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxtQ0FBVyxHQUFYO1FBQUEsaUJBWUM7UUFYRyxPQUFPLENBQUMsT0FBTyxDQUFDO1lBQ1osS0FBSyxFQUFFLGNBQWM7WUFDckIsT0FBTyxFQUFFLGVBQWU7WUFDeEIsWUFBWSxFQUFFLGFBQWE7WUFDM0IsZ0JBQWdCLEVBQUUsUUFBUTtTQUM3QixDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsTUFBTTtZQUNWLDZCQUE2QjtZQUM3QixJQUFJLE1BQU0sSUFBSSxJQUFJLEVBQUM7Z0JBQ2YsS0FBSSxDQUFDLGdCQUFnQixDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsQ0FBQzthQUNoRDtRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQXJLUSxhQUFhO1FBTnpCLGdCQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsTUFBTTtZQUNoQixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsV0FBVyxFQUFFLHVCQUF1QjtZQUNwQyxTQUFTLEVBQUUsQ0FBQyxzQkFBc0IsQ0FBQztTQUN0QyxDQUFDO3lDQVd3Qyx1Q0FBZ0IsRUFBZ0IsV0FBSSxFQUFrQixlQUFNLEVBQXdCLGlDQUFrQixFQUE0Qix1QkFBZ0IsRUFBdUIsMEJBQVc7T0FWak4sYUFBYSxDQXNLekI7SUFBRCxvQkFBQztDQUFBLEFBdEtELElBc0tDO0FBdEtZLHNDQUFhIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtBZnRlclZpZXdJbml0LCBDb21wb25lbnQsIEVsZW1lbnRSZWYsIE9uSW5pdCwgVmlld0NoaWxkLCBWaWV3Q29udGFpbmVyUmVmfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHtQYWdlfSBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy91aS9wYWdlXCI7XG5pbXBvcnQge1JvdXRlcn0gZnJvbSBcIkBhbmd1bGFyL3JvdXRlclwiO1xuaW1wb3J0IHtNb2RhbERpYWxvZ1NlcnZpY2UsIE1vZGFsRGlhbG9nT3B0aW9uc30gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL21vZGFsLWRpYWxvZ1wiO1xuaW1wb3J0IHtHYW1lU2VydmljZX0gZnJvbSBcIn4vc2VydmljZXMvZ2FtZS5zZXJ2aWNlXCI7XG5pbXBvcnQge3NldEludGVydmFsLCBjbGVhckludGVydmFsfSBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy90aW1lclwiO1xuaW1wb3J0IHtDYXJkfSBmcm9tIFwifi9tb2RlbC9jYXJkLm1vZGVsXCI7XG5pbXBvcnQge2ZvckVhY2h9IGZyb20gXCJAYW5ndWxhci9yb3V0ZXIvc3JjL3V0aWxzL2NvbGxlY3Rpb25cIjtcbmltcG9ydCAqIGFzIGRpYWxvZ3MgZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvZGlhbG9nc1wiO1xuaW1wb3J0IHtHYW1lU2Vzc2lvbn0gZnJvbSBcIn4vbW9kZWwvZ2FtZVNlc3Npb24ubW9kZWxcIjtcbmltcG9ydCB7Um91dGVyRXh0ZW5zaW9uc30gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyXCI7XG5cbnZhciBhcHBTZXR0aW5ncyA9IHJlcXVpcmUoJ2FwcGxpY2F0aW9uLXNldHRpbmdzJyk7XG52YXIgdmlldyA9IHJlcXVpcmUoXCJ1aS9jb3JlL3ZpZXdcIik7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiBcIkdhbWVcIixcbiAgICBtb2R1bGVJZDogbW9kdWxlLmlkLFxuICAgIHRlbXBsYXRlVXJsOiBcIi4vZ2FtZS5jb21wb25lbnQuaHRtbFwiLFxuICAgIHN0eWxlVXJsczogWycuL2dhbWUuY29tcG9uZW50LmNzcyddXG59KVxuZXhwb3J0IGNsYXNzIEdhbWVDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICAgIHRva2VuOiBzdHJpbmc7XG4gICAgY291bnRPZlJvd3M6IHN0cmluZztcbiAgICBpdGVtc1RvTG9hZCA9IFtdO1xuICAgIGNhcmRMaXN0ID0gW107XG4gICAgc3RheU9uU2Vzc2lvbiA9IHRydWU7XG4gICAgc2Vzc2lvbklkOiBudW1iZXI7XG4gICAgZ2FtZVNlc3Npb24gPSBuZXcgR2FtZVNlc3Npb24oKTtcblxuXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSByb3V0ZXJFeHRlbnNpb25zOiBSb3V0ZXJFeHRlbnNpb25zLCBwcml2YXRlIHBhZ2U6IFBhZ2UsIHByaXZhdGUgcm91dGVyOiBSb3V0ZXIsIHByaXZhdGUgbW9kYWxTZXJ2aWNlOiBNb2RhbERpYWxvZ1NlcnZpY2UsIHByaXZhdGUgdmlld0NvbnRhaW5lclJlZjogVmlld0NvbnRhaW5lclJlZiwgcHJpdmF0ZSBnYW1lU2VydmljZTogR2FtZVNlcnZpY2UpIHtcbiAgICAgICAgdGhpcy5wYWdlLmVuYWJsZVN3aXBlQmFja05hdmlnYXRpb24gPSBmYWxzZTtcbiAgICB9XG5cbiAgICBjcmVhdGVSb3dzKCkge1xuICAgICAgICB2YXIgY291bnQgPSBcIlwiO1xuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8ICgodGhpcy5pdGVtc1RvTG9hZC5sZW5ndGggKiAyKSArIDEpOyBpKyspIHtcbiAgICAgICAgICAgIGlmIChpID09ICh0aGlzLml0ZW1zVG9Mb2FkLmxlbmd0aCoyKSkge1xuICAgICAgICAgICAgICAgIGNvdW50ID0gY291bnQgKyBcIipcIjtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgY291bnQgPSBjb3VudCArIFwiKixcIjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICB0aGlzLmNvdW50T2ZSb3dzID0gY291bnQ7XG4gICAgfVxuXG4gICAgY2xpY2sobWVudUlkOiBudW1iZXIpIHtcbiAgICAgICAgdGhpcy5nYW1lU2VydmljZS5tYWtlVHVybih0aGlzLnRva2VuLCB0aGlzLml0ZW1zVG9Mb2FkW21lbnVJZF0uaWQpLnN1YnNjcmliZShcbiAgICAgICAgICAgIHJlc3VsdCA9PiB7XG4gICAgICAgICAgICAgICAgdGhpcy5hbmltYXRlQ2FyZChtZW51SWQsIHJlc3VsdFtcImNhcmRMaXN0XCJdW21lbnVJZF1bXCJwcmlvcml0eVwiXSk7XG4gICAgICAgICAgICB9LCBlcnJvciA9PiB7XG4gICAgICAgICAgICAgICAgaWYoZXJyb3Iuc3RhdHVzID09IDUwMCl7XG4gICAgICAgICAgICAgICAgICAgIGRpYWxvZ3MuY29uZmlybSh7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aXRsZTogXCJUT0tFTiBIQVMgRVhQSVJFRFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgbWVzc2FnZTogXCJZb3UgaGF2ZSB0byBsb2cgaW4gYWdhaW4uXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICBva0J1dHRvblRleHQ6IFwiR28gdG8gbG9naW5cIlxuICAgICAgICAgICAgICAgICAgICB9KS50aGVuKHIgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGVCeVVybCgnL2xvZ2luJyk7XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH1lbHNle1xuICAgICAgICAgICAgICAgICAgICBkaWFsb2dzLmNvbmZpcm0oe1xuICAgICAgICAgICAgICAgICAgICAgICAgdGl0bGU6IFwiUExFQVNFIFdBSVRcIixcbiAgICAgICAgICAgICAgICAgICAgICAgIG1lc3NhZ2U6IGVycm9yLmVycm9yLm1lc3NhZ2UsXG4gICAgICAgICAgICAgICAgICAgICAgICBva0J1dHRvblRleHQ6IFwiT0tcIlxuICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgKTtcbiAgICB9XG4gICAgbmdPbkluaXQoKTogdm9pZCB7XG4gICAgICAgIHRoaXMudG9rZW4gPSBhcHBTZXR0aW5ncy5nZXRTdHJpbmcoXCJ0b2tlblwiKTtcbiAgICAgICAgdGhpcy5zZXNzaW9uSWQgPSBhcHBTZXR0aW5ncy5nZXRTdHJpbmcoJ3Nlc3Npb25JZCcpO1xuICAgICAgICB0aGlzLmdldENhcmRzKHRoaXMuc2Vzc2lvbklkKTtcbiAgICAgICAgdGhpcy5yZXBlYXRlcjtcbiAgICB9XG5cbiAgICByZXBlYXRlciA9IHNldEludGVydmFsKCgpID0+IHtcbiAgICAgICAgaWYodGhpcy5zdGF5T25TZXNzaW9uKXtcbiAgICAgICAgICAgIHRoaXMudXBkYXRlU2Vzc2lvbih0aGlzLnNlc3Npb25JZCk7XG4gICAgICAgIH1cbiAgICB9LCAzMDAwKTtcblxuXG4gICAgZ2V0Q2FyZHMoc2Vzc2lvbklkOiBudW1iZXIpIHtcbiAgICAgICAgdGhpcy5nYW1lU2VydmljZS5nZXRTZXNzaW9uKHRoaXMudG9rZW4sIHRoaXMuc2Vzc2lvbklkKS5zdWJzY3JpYmUocmVzdWx0ID0+IHtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKHJlc3VsdFtcImNhcmRMaXN0XCJdKTtcbiAgICAgICAgICAgIGlmKHJlc3VsdFtcImNhcmRMaXN0XCJdLnRvU3RyaW5nKCkuc3BsaXQoJywnKS5sZW5ndGggPiAxKXtcbiAgICAgICAgICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHJlc3VsdFtcImNhcmRMaXN0XCJdLnRvU3RyaW5nKCkuc3BsaXQoJywnKS5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICAgICAgICB2YXIgY2FyZCA9IG5ldyBDYXJkO1xuICAgICAgICAgICAgICAgICAgICBjYXJkID0gcmVzdWx0W1wiY2FyZExpc3RcIl1baV07XG4gICAgICAgICAgICAgICAgICAgIGNhcmQubGFiZWxJZCA9IFwiY2FyZFwiICsgaTtcbiAgICAgICAgICAgICAgICAgICAgY2FyZC5jb2xvciA9IHRoaXMuY29sb3JjaGFuZ2VyKCk7XG5cbiAgICAgICAgICAgICAgICAgICAgdGhpcy5pdGVtc1RvTG9hZC5wdXNoKGNhcmQpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB0aGlzLmNyZWF0ZVJvd3MoKVxuICAgICAgICAgICAgfWVsc2V7XG4gICAgICAgICAgICAgICAgZGlhbG9ncy5jb25maXJtKHtcbiAgICAgICAgICAgICAgICAgICAgdGl0bGU6IFwiTk8gQ0FSRFMgU0VMRUNURURcIixcbiAgICAgICAgICAgICAgICAgICAgbWVzc2FnZTogXCJZb3UgY2Fubm90IHN0YXJ0IGEgc2Vzc2lvbiB3aXRob3V0IGNhcmRzLlwiLFxuICAgICAgICAgICAgICAgICAgICBva0J1dHRvblRleHQ6IFwiR28gdG8gaG9tZVwiXG4gICAgICAgICAgICAgICAgfSkudGhlbihyID0+IHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGVCeVVybCgnL2hvbWUnKTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH19LCBlcnJvciA9PntcbiAgICAgICAgICAgIGlmKGVycm9yLnN0YXR1cyA9PSA1MDApe1xuICAgICAgICAgICAgICAgIGRpYWxvZ3MuY29uZmlybSh7XG4gICAgICAgICAgICAgICAgICAgIHRpdGxlOiBcIlRPS0VOIEhBUyBFWFBJUkVEXCIsXG4gICAgICAgICAgICAgICAgICAgIG1lc3NhZ2U6IFwiWW91IGhhdmUgdG8gbG9nIGluIGFnYWluLlwiLFxuICAgICAgICAgICAgICAgICAgICBva0J1dHRvblRleHQ6IFwiR28gdG8gbG9naW5cIlxuICAgICAgICAgICAgICAgIH0pLnRoZW4ociA9PiB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlQnlVcmwoJy9sb2dpbicpO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfWVsc2V7XG4gICAgICAgICAgICAgICAgZGlhbG9ncy5jb25maXJtKHtcbiAgICAgICAgICAgICAgICAgICAgdGl0bGU6IFwiUExFQVNFIFdBSVRcIixcbiAgICAgICAgICAgICAgICAgICAgbWVzc2FnZTogZXJyb3IuZXJyb3IubWVzc2FnZSxcbiAgICAgICAgICAgICAgICAgICAgb2tCdXR0b25UZXh0OiBcIk9LXCJcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICB1cGRhdGVTZXNzaW9uKHNlc3Npb25JZDogbnVtYmVyKXtcbiAgICAgICAgdGhpcy5nYW1lU2VydmljZS5nZXRTZXNzaW9uKHRoaXMudG9rZW4sIHNlc3Npb25JZCkuc3Vic2NyaWJlKHJlc3VsdCA9PiB7XG4gICAgICAgICAgICBpZihyZXN1bHRbXCJnYW1lT3ZlclwiXSl7XG4gICAgICAgICAgICAgICAgdGhpcy5zdGF5T25TZXNzaW9uID0gZmFsc2U7XG4gICAgICAgICAgICAgICAgZGlhbG9ncy5wcm9tcHQoe1xuICAgICAgICAgICAgICAgICAgICB0aXRsZTogXCJHYW1lIG92ZXIhXCIsXG4gICAgICAgICAgICAgICAgICAgIG1lc3NhZ2U6IFwiTWF4IHByaW9yaXR5IGhhcyBiZWVuIHJlYWNoZWQuXCIsXG4gICAgICAgICAgICAgICAgICAgIG9rQnV0dG9uVGV4dDogXCJFbmQgc2Vzc2lvblwiXG4gICAgICAgICAgICAgICAgfSkudGhlbihyID0+IHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGVCeVVybCgnL2hvbWUnKTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1lbHNle1xuICAgICAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5pdGVtc1RvTG9hZC5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmFuaW1hdGVDYXJkKGkscmVzdWx0W1wiY2FyZExpc3RcIl1baV1bXCJwcmlvcml0eVwiXSk7XG4gICAgICAgICAgICAgICAgICAgIGlmKHJlc3VsdFtcImNhcmRMaXN0XCJdW2ldW1wicHJpb3JpdHlcIl0gPiB0aGlzLml0ZW1zVG9Mb2FkW2ldLnByaW9yaXR5KXtcbiAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmFuaW1hdGVDYXJkKGkscmVzdWx0W1wiY2FyZExpc3RcIl1baV1bXCJwcmlvcml0eVwiXSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5pdGVtc1RvTG9hZFtpXS5wcmlvcml0eSA9IHJlc3VsdFtcImNhcmRMaXN0XCJdW2ldW1wicHJpb3JpdHlcIl07XG4gICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9LCBlcnJvciA9PiB7XG4gICAgICAgICAgICBpZihlcnJvci5zdGF0dXMgPT0gNTAwKXtcbiAgICAgICAgICAgICAgICBkaWFsb2dzLmNvbmZpcm0oe1xuICAgICAgICAgICAgICAgICAgICB0aXRsZTogXCJUT0tFTiBIQVMgRVhQSVJFRFwiLFxuICAgICAgICAgICAgICAgICAgICBtZXNzYWdlOiBcIllvdSBoYXZlIHRvIGxvZyBpbiBhZ2Fpbi5cIixcbiAgICAgICAgICAgICAgICAgICAgb2tCdXR0b25UZXh0OiBcIkdvIHRvIGxvZ2luXCJcbiAgICAgICAgICAgICAgICB9KS50aGVuKHIgPT4ge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZUJ5VXJsKCcvbG9naW4nKTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1lbHNle1xuICAgICAgICAgICAgICAgIGRpYWxvZ3MuY29uZmlybSh7XG4gICAgICAgICAgICAgICAgICAgIHRpdGxlOiBcIlBMRUFTRSBXQUlUXCIsXG4gICAgICAgICAgICAgICAgICAgIG1lc3NhZ2U6IGVycm9yLmVycm9yLm1lc3NhZ2UsXG4gICAgICAgICAgICAgICAgICAgIG9rQnV0dG9uVGV4dDogXCJPS1wiXG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgY29sb3JjaGFuZ2VyKCk6IHN0cmluZ3tcbiAgICAgICAgcmV0dXJuIFwicmdiKFwiICsgTWF0aC5yb3VuZChNYXRoLnJhbmRvbSgpICogMjUwKSArIFwiLFwiICsgTWF0aC5yb3VuZChNYXRoLnJhbmRvbSgpICogMjUwKStcIixcIiArIE1hdGgucm91bmQoTWF0aC5yYW5kb20oKSAqIDI1MCkgK1wiKVwiO1xuICAgIH1cblxuICAgIGFuaW1hdGVDYXJkKGxpc3RJZDogbnVtYmVyLCBwcmlvcml0eTogbnVtYmVyKXtcbiAgICAgICAgdmlldy5nZXRWaWV3QnlJZCh0aGlzLnBhZ2UsIHRoaXMuaXRlbXNUb0xvYWRbbGlzdElkXS5sYWJlbElkKS5hbmltYXRlKHtcbiAgICAgICAgICAgIHRyYW5zbGF0ZToge3g6IChwcmlvcml0eSAqIDE1KSAsIHk6IDB9LFxuICAgICAgICAgICAgZHVyYXRpb246IDEwMDAsXG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIHN0b3BTZXNzaW9uKCl7XG4gICAgICAgIGRpYWxvZ3MuY29uZmlybSh7XG4gICAgICAgICAgICB0aXRsZTogXCJTdG9wIHNlc3Npb25cIixcbiAgICAgICAgICAgIG1lc3NhZ2U6IFwiQXJlIHlvdSBzdXJlP1wiLFxuICAgICAgICAgICAgb2tCdXR0b25UZXh0OiBcIkdvIHRvIGxvZ2luXCIsXG4gICAgICAgICAgICBjYW5jZWxCdXR0b25UZXh0OiBcIkNhbmNlbFwiXG4gICAgICAgIH0pLnRoZW4ocmVzdWx0ID0+IHtcbiAgICAgICAgICAgIC8vIHJlc3VsdCBhcmd1bWVudCBpcyBib29sZWFuXG4gICAgICAgICAgICBpZiAocmVzdWx0ID09IHRydWUpe1xuICAgICAgICAgICAgICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZUJ5VXJsKCcvaG9tZScpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9XG59XG5cblxuXG5cblxuXG5cblxuXG4iXX0=